﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReqApp.Repositories;
using ReqApp.BusinessLogic.Models;
using ReqApp.BusinessLogic.Services;
using ReqApp.Presentation.Requests;

namespace ReqApp.Controllers
{
    [Authorize]
    public class RequestsController : Controller
    {
        //TODO: использовать встраивание зависимостей
        private IRequestRepository requestRepository = new ReqApp.Repositories.EF.RequestRepository();
        private IRoleRepository roleRepository = new ReqApp.Repositories.EF.RoleRepository();
        private IResolutionRepository resolutionRepository = new ReqApp.Repositories.EF.ResolutionRepository();
        
        //TODO: использовать Automapper
        private ShowViewModelConverter showModelConverter = new ShowViewModelConverter();
        private EditViewModelConverter editModelConverter = new EditViewModelConverter();
        
        private RequestDispatcher requestDispatcher = new RequestDispatcher();

        // GET: /Requests/       
        [Authorize(Roles = "Секретарь,Руководитель")]
        public ActionResult Index()
        {                        
            // TODO: рефакторинг
            // TODO: использовать разбивку на страницы

            bool desc = Request.Params["sortOrder"] != null && Request.Params["sortOrder"].ToLower() == "desc";
            
            var requests = (Request.Params["sortBy"] != null && 
                Request.Params["sortBy"].ToLower() == "resolved") ?                            
                requestRepository.FindAllOrderResolved(desc) :            
                requestRepository.All;
            
            return View(showModelConverter.ConvertToViewModels(requests));
        }

        // GET: /Requests/       
        [Authorize(Roles="Руководитель,Бухгалтер,Менеджер закупок,Аналитик")]
        public ActionResult AssignedToMe()
        {
            // TODO: рефакторинг
            // TODO: использовать разбивку на страницы

            var role = roleRepository.FindByName(User.Identity.Name);               
            if (null == role)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            
            bool desc = Request.Params["sortOrder"] != null && Request.Params["sortOrder"].ToLower() == "desc";

            IEnumerable<Request> requests = null;

            if (Request.Params["sortBy"] != null)             
            { 
                if (Request.Params["sortBy"].ToLower() == "resolved")
                {
                    requests = requestRepository.FindByRoleOrderResolved(role, desc);
                }
                else if (Request.Params["sortBy"].ToLower() == "resolvedbyme")
                {
                    requests = requestRepository.FindByRoleOrderResolvedBy(role, desc);
                }
            }
            if (null == requests)
            { 
                requests = requestRepository.FindByRole(role);
            }

            requests = requests.Select(r => BuildRequest(r));

            return View(showModelConverter.ConvertToViewModels(requests));
        }

        // GET: /Requests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Request request = CreateRequest(id.Value);
            if (request == null)
            {
                return HttpNotFound();
            }            

            return View(showModelConverter.ConvertToViewModel(request));
        }

        // GET: /Requests/Create
        [Authorize(Roles = "Секретарь")]
        public ActionResult Create()
        {            
            return View(editModelConverter.ConvertToViewModel(new Request()));
        }

        // POST: /Requests/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Секретарь")]
        public ActionResult Create(EditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var roleIDs = viewModel.RoleOptions
                    .Where(o => o.Selected)
                    .Select(o => o.Value)
                    .ToArray();

                requestDispatcher.RegisterRequest(
                    viewModel.Title, 
                    viewModel.Desc, 
                    viewModel.Priority,
                    viewModel.DueDate.Value, 
                    roleIDs);

                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

           
        // TODO: перенести в отдельный класс
        private Request CreateRequest(int requestID) 
        {
            var request = requestRepository.GetByID(requestID);
            return BuildRequest(request);            
        }

        // TODO: перенести в отдельный класс
        private Request BuildRequest(Request request)
        {            
            if (request != null)
            {
                requestRepository.LoadRoles(request);
                request.Resolutions = resolutionRepository.FindByRequest(request.ID);
            }
            return request;
        }
    }
}
