﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ReqApp.Controllers
{
    // Упрощенный контроллер сеансов пользователей (пользовтель соответствует роли)
    public class SessionsController : Controller
    {        
        public ActionResult LogIn(string name)
        {
            if (Roles.GetAllRoles().Any(r => r == name))
                {
                SetupAuthTicket(name);
                }
            return RedirectToAction("Index", "Home");            
        }
        
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        protected void SetupAuthTicket(string name) 
        {   
            // Срок действия билета = 1 час
            var ticket = new FormsAuthenticationTicket(name, true, 60);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
            cookie.Expires = ticket.Expiration;
            Response.Cookies.Add(cookie);            
        }
	}
}