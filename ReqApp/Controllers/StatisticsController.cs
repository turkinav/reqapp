﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReqApp.BusinessLogic.Services;
using ReqApp.Presentation;

namespace ReqApp.Controllers
{    
    public class StatisticsController : Controller
    {
        private StatisticsCollector collector = new StatisticsCollector();

        // GET: /Statistics
        [Authorize(Roles = "Руководитель")]
        public ActionResult Index(DateTime dateFrom, DateTime dateTo)
        {
            // TODO: перенести во всомогательный класс
            dateFrom = dateFrom.Date;
            dateFrom = dateFrom < StatisticsCollector.MinDate ? StatisticsCollector.MinDate : dateFrom;

            dateTo= dateTo.Date;
            dateTo = StatisticsCollector.MaxDate < dateTo ? StatisticsCollector.MaxDate : dateTo; 

            var viewModel = new ResolutionStatsViewModel
            {
                DateFrom = dateFrom,
                DateTo = dateTo,
                Statistics = collector.GetStatistics(dateFrom, dateTo)
            };
            
            return View(viewModel);
        }

        // GET: /Statistics/Create
        [Authorize(Roles = "Руководитель")]
        public ActionResult Create()
        {
            var viewModel = new ResolutionStatsViewModel
            {
                DateTo = DateTime.Today,
                DateFrom = new DateTime(DateTime.Today.Year, 1, 1)
            };

            return View(viewModel);
        }

        // POST: /Statistics/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Руководитель")]
        public ActionResult Create(ResolutionStatsViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index", new { dateFrom = viewModel.DateFrom, dateTo = viewModel.DateTo });
            }

            return View(viewModel);
        }

	}


}