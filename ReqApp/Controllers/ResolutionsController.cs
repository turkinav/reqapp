﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data.Entity;
using ReqApp.Repositories;
using ReqApp.BusinessLogic.Models;
using ReqApp.BusinessLogic.Services;
using ReqApp.Presentation.Resolutions;

namespace ReqApp.Controllers
{
    [Authorize]
    public class ResolutionsController : Controller
    {
        private IRoleRepository roleRepository = new ReqApp.Repositories.EF.RoleRepository();
        private IRequestRepository requestRepository = new ReqApp.Repositories.EF.RequestRepository();
        private IResolutionRepository resolutionRepository = new ReqApp.Repositories.EF.ResolutionRepository();        
        private ViewModelConverter viewModelConverter = new ViewModelConverter();

        private RequestDispatcher requestDispatcher = new RequestDispatcher();

        // GET: /Resolutions/Create        
        public ActionResult Create(int requestID)
        {
            var request = requestRepository.GetByID(requestID);
            var role = roleRepository.FindByName(User.Identity.Name);

            if ((null == request) || (null == role))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var resolution = new Resolution { Request = request, Role = role };
            
            return View(viewModelConverter.ConvertToViewModel(resolution));            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                requestDispatcher.RegisterResolution(
                    viewModel.RequestID, 
                    viewModel.RoleID, 
                    viewModel.Result.Value, 
                    viewModel.Desc);

                return RedirectToAction("Details", "Requests", new { id = viewModel.RequestID });
            }
            return View(viewModel);
        }
    }
}