﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ReqApp.BusinessLogic.Models;

namespace ReqApp.Presentation.Resolutions
{
    public class ViewModel
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name="Решение")]        
        public ResolutionResult? Result { get; set; }

        [Display(Name = "Комментарий")]
        [StringLength(255)]
        public string Desc { get; set; }
        
        [Required]         
        public virtual int RoleID { get; set; }

        [Required]        
        [Display(Name = "Время принятия")]
        [DataType(DataType.DateTime)]
        public DateTime RegistrationTime { get; set; }

        [Required]
        public int RequestID { get; set; }
        
        [Display(Name = "Заявка")]
        public string RequestTitle { get; set; }
        
        [Display(Name = "Описание заявки")]
        public string RequestDesc { get; set; }

        [Display(Name = "Срок выполнения")]
        public DateTime RequestDueDate { get; set; }
    }

    public class ViewModelConverter 
    {       
        public ViewModel ConvertToViewModel(Resolution resolution) 
        {
            return new ViewModel 
            { 
                ID = resolution.ID,
                RequestID = resolution.Request.ID,
                Result = resolution.Result,
                Desc = resolution.Desc,
                RegistrationTime = resolution.RegistrationTime,
                RoleID = resolution.Role.ID,
                RequestTitle = resolution.Request.Title,
                RequestDesc = resolution.Request.Desc,
                RequestDueDate = resolution.Request.DueDate.Value
            };
        }
    }
}