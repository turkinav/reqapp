﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;
using ReqApp.BusinessLogic.Models;
using ReqApp.Repositories;

namespace ReqApp.Presentation.Requests
{
    public class ShowViewModel
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Название")]
        [StringLength(60, MinimumLength = 3)]
        public string Title { get; set; }

        [Display(Name = "Описание")]
        [StringLength(255)]
        public string Desc { get; set; }

        [Display(Name = "Приоритет")]
        public RequestPriority Priority { get; set; }

        [Display(Name = "Время регистрации")]
        [DataType(DataType.DateTime)]
        public DateTime RegistrationTime { get; set; }

        [Required]        
        [Display(Name = "Срок рассмотрения")]
        [DataType(DataType.Date)]
        public DateTime? DueDate { get; set; }

        [Display(Name = "Рассмотрена")]
        public bool Resolved { get; set; }

        [Display(Name = "Рассмотрена мной")]
        public bool ResolvedByMe { get; set; }
        
        public bool CanResolve { get; set; }

        [Display(Name = "Роли и решения")]
        public IEnumerable<RoleResolutionViewModel> RoleResolutions { get; set; }
    }

    //TODO: использовать Automapper?
    public class ShowViewModelConverter
    {
        private IRoleRepository roleRepository = new ReqApp.Repositories.EF.RoleRepository();
        private RoleHelper roleHelper = new RoleHelper();

        public ShowViewModel ConvertToViewModel(Request request) 
        {
            
            List<RoleResolutionViewModel> resolutions = new List<RoleResolutionViewModel>();
            // Формирование модели для отображения решений по ролям
            foreach (var role in request.Roles.OrderBy(r => r.Name))
            {
                var resolution = request.Resolutions.FirstOrDefault(res => res.RoleID == role.ID);
                var resolutionViewModel = new RoleResolutionViewModel {RoleName = role.Name };
                if (resolution != null)
                {
                    resolutionViewModel.Result = resolution.Result;
                    resolutionViewModel.Desc = resolution.Desc;
                }
                resolutions.Add(resolutionViewModel);
            }
            
            return new ShowViewModel
            {
                ID = request.ID,
                Title = request.Title,
                Desc = request.Desc,
                Priority = (RequestPriority)request.Priority,
                RegistrationTime = request.RegistrationTime,
                DueDate = request.DueDate,
                Resolved = request.Resolved,                        
                ResolvedByMe = request.IsResolvedBy(roleHelper.CurrentRole),
                CanResolve = request.CanBeResolvedBy(roleHelper.CurrentRole),                
                RoleResolutions = resolutions
            };
        }

        public IEnumerable<ShowViewModel> ConvertToViewModels(IEnumerable<Request> requests)
        {
            return requests.Select(r => ConvertToViewModel(r));
        }
    }
}