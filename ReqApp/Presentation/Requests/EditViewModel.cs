﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ReqApp.BusinessLogic.Models;
using ReqApp.Repositories;

namespace ReqApp.Presentation.Requests
{
    public class EditViewModel
    {
        public int ID { get; set; }

        [Required]        
        [Display(Name = "Название")]
        [StringLength(60, MinimumLength = 3)]
        public string Title { get; set; }

        [Display(Name = "Описание")]
        [StringLength(255)]
        public string Desc { get; set; }

        [Display(Name = "Приоритет")]
        public RequestPriority Priority { get; set; }

        [Display(Name = "Время регистрации")]
        [DataType(DataType.DateTime)]
        public DateTime RegistrationTime { get; set; }

        [Required]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        [Display(Name = "Срок рассмотрения")]
        [DataType(DataType.Date)]
        public DateTime? DueDate { get; set; }

        [Display(Name = "Рассмотрена")]
        public bool Resolved { get; set; }

        [Display(Name = "Роли")]
        public IList<SelectOptionViewModel> RoleOptions { get; set; }
    }

    //TODO: использовать Automapper
    public class EditViewModelConverter
    {
        private IRoleRepository roleRepository = new ReqApp.Repositories.EF.RoleRepository();

        public EditViewModel ConvertToViewModel(Request request)
        {
            return new EditViewModel
            {
                ID = request.ID,
                Title = request.Title,
                Desc = request.Desc,
                Priority = (RequestPriority)request.Priority,
                RegistrationTime = request.RegistrationTime,
                DueDate = request.DueDate,
                Resolved = request.Resolved,
                RoleOptions = CreateRoleOptions(request.Roles)
            };
        }

        public IEnumerable<EditViewModel> CreateViewModels(IEnumerable<Request> requests)
        {
            return requests.Select(r => ConvertToViewModel(r));
        }

        
        public Request CreateRequest(EditViewModel viewModel)
        {
            var request = new Request
            {
                ID = viewModel.ID,
                Title = viewModel.Title,
                Desc = viewModel.Desc,
                Priority = viewModel.Priority,
                DueDate = viewModel.DueDate.Value
            };
            
            var roleIDs = viewModel.RoleOptions.Where(o => o.Selected).Select(o => o.Value).ToArray();
            var roles = roleRepository.FindByIDs(roleIDs).ToList();
            roles.Add(roleRepository.FindByName("Руководитель"));

            request.Roles = roles;

            return request;
        }

        private List<SelectOptionViewModel> CreateRoleOptions(IEnumerable<Role> requestRoles)
        {
            return roleRepository.All
                .Where(r => r.Name != "Секретарь")
                .Select(r =>
                    new SelectOptionViewModel
                    {
                        Value = r.ID,
                        Name = r.Name,
                        Selected = (r.Name == "Руководитель") || (requestRoles != null && requestRoles.Any(rr => rr == r)),
                        Disabled = (r.Name == "Руководитель")
                    }).ToList();
        }
    }
}