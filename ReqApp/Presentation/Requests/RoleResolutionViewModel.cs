﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ReqApp.BusinessLogic.Models;

namespace ReqApp.Presentation.Requests
{
    public class RoleResolutionViewModel
    {
        [Display(Name = "Роль")]        
        public string RoleName { get; set; }

        [Display(Name="Решение")]        
        public ResolutionResult? Result { get; set; }

        [Display(Name = "Комментарий")]
        public string Desc { get; set; }
    }
}