﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReqApp.BusinessLogic.Models;
using ReqApp.Repositories;

namespace ReqApp.Presentation
{
    public class RoleHelper
    {
        private IRoleRepository roleRepository = new ReqApp.Repositories.EF.RoleRepository();
        private Role currentRole;
        
        public Role CurrentRole
        {
            get
            {
                if (null == currentRole)
                {
                    var userName = HttpContext.Current.User.Identity.Name;
                    currentRole = roleRepository.FindByName(userName);
                }
                return currentRole;
            }
        }
    }
}