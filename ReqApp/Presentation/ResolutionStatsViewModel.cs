﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using ReqApp.BusinessLogic.Models;

namespace ReqApp.Presentation
{
    public class ResolutionStatsViewModel
    {
        [Display(Name="Начальная дата:")]
        [DataType(DataType.Date)]
        public DateTime DateFrom { get; set; }

        [Display(Name = "Конечная дата:")]
        [DataType(DataType.Date)]
        public DateTime DateTo { get; set; }
        public IEnumerable<ResolutionStats> Statistics { get; set; }
    }
}