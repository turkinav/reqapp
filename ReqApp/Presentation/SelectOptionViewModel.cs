﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReqApp.Presentation
{
    public class SelectOptionViewModel
    {
        public string Name { get; set; }
        public int Value { get; set; }
        public bool Selected { get; set; }
        public bool Disabled { get; set; }
    }
}