﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReqApp.BusinessLogic.Models;

namespace ReqApp.Repositories
{
    public interface IResolutionRepository: IRepository<Resolution>
    {
        IEnumerable<Resolution> FindByRequest(int requestID);
    }
}
