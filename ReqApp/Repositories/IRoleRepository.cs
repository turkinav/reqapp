﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReqApp.BusinessLogic.Models;

namespace ReqApp.Repositories
{
    public interface IRoleRepository: IRepository<Role>
    {
        IEnumerable<Role> FindByIDs(int[] roleIDs);
        Role FindByName(string name);
    }
}