﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReqApp.BusinessLogic.Models;

namespace ReqApp.Repositories
{
    public interface IRequestRepository: IRepository<Request>
    {        
        void LoadRoles(Request request);
        IEnumerable<Request> FindByRole(Role role);
        // TODO: использовать спецификации запросов
        IEnumerable<Request> FindAllOrderResolved(bool desc);

        IEnumerable<Request> FindByRoleOrderResolved(Role role, bool desc);
        IEnumerable<Request> FindByRoleOrderResolvedBy(Role role, bool desc);
    }
}