﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReqApp.Repositories
{
    public interface IRepository<ItemType>
    {
        ItemType GetByID(int id);
        IEnumerable<ItemType> All { get; }        

        void Add(ItemType item);        
        void Update(ItemType item);
        void Remove(ItemType item);        
    }
}