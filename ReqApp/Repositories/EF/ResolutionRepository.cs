﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReqApp.BusinessLogic.Models;
using ReqApp.Infrastructure.Persistence.EF;

namespace ReqApp.Repositories.EF
{
    public class ResolutionRepository: IResolutionRepository
    {
        public Resolution GetByID(int id)
        {
            using (var db = new ReqAppContext())
            {
                return db.Resolutions.Find(id);
            }
        }

        public void Add(Resolution resolution)
        {
            using (var db = new ReqAppContext())
            {
                db.Resolutions.Add(resolution);
                db.SaveChanges();
            }
        }

        public void Remove(Resolution resolution)
        {
            //TODO
            throw new NotImplementedException();
        }

        public void Update(Resolution resolution)
        {
            //TODO
            throw new NotImplementedException();
        }


        public IEnumerable<Resolution> All
        {
            get
            {
                using (var db = new ReqAppContext())
                {
                    return db.Resolutions.ToList();
                }
            }
        }

        public IEnumerable<Resolution> FindByRequest(int requestID)
        {
            using (var db = new ReqAppContext())
            {
                return db.Resolutions.Where(r => r.RequestID == requestID).ToList();
            }        
        }

    }
}