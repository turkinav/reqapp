﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ReqApp.BusinessLogic.Models;
using ReqApp.Infrastructure.Persistence.EF;

namespace ReqApp.Repositories.EF
{
    public class RoleRepository : IRoleRepository
    {
        public Role GetByID(int id)
        {
            using (var db = new ReqAppContext())
            {
                return db.Roles.Find(id);
            }
        }

        public void Add(Role role)
        {
            using (var db = new ReqAppContext())
            {
                db.Roles.Add(role);
                db.SaveChanges();
            }
        }

        public void Update(Role role)
        {
            //TODO
            throw new NotImplementedException();
        }

        public void Remove(Role resolution)
        {
            //TODO
            throw new NotImplementedException();
        }



        public IEnumerable<Role> All
        {
            get
            {
                using (var db = new ReqAppContext())
                {
                    return db.Roles.ToList();
                }
            }
        }


        public IEnumerable<Role> FindByIDs(int[] ids)
        {            
            using (var db = new ReqAppContext())
            {
                return db.Roles.Where(r => ids.Contains(r.ID)).ToList();
            }
        }

        public Role FindByName(string name)
        {
            using (var db = new ReqAppContext())
            {
                return db.Roles.FirstOrDefault(r => r.Name == name);
            }
        }
    }
}