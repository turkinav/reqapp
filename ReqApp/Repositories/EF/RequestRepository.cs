﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Text;
using ReqApp.BusinessLogic.Models;
using ReqApp.Infrastructure.Persistence.EF;

namespace ReqApp.Repositories.EF
{
    public class RequestRepository : IRequestRepository
    {
        public Request GetByID(int id) 
        {
            using (var db = new ReqAppContext()) 
            {
                return db.Requests.Find(id);
            }
        }

        public void Add(Request request)
        {
            using (var db = new ReqAppContext())
            {
                // Т.к. ID заявки становится известным только после сохранения в БД, сохраняем изменения в 2 этапа.
                // TODO: использовать транзакцию                
                db.Requests.Add(request);                                
                db.SaveChanges();

                db.RoleRequests.AddRange(
                    request.Roles.Select(r => new RoleRequest { RequestID = request.ID, RoleID = r.ID }));
                db.SaveChanges();
            }
        }

        public void Update(Request request)
        {
            using (var db = new ReqAppContext())
            {
                db.Entry(request).State = EntityState.Modified;
                db.SaveChanges();
            }            
        }


        public void Remove(Request request) 
        {
            //TODO
            throw new NotImplementedException();
        }

        public IEnumerable<Request> All
        {
            get
            {
                using (var db = new ReqAppContext())
                {
                                        
                    return db.Requests.ToList();
                }
            }
        }

        public IEnumerable<Request> FindByRole(Role role) 
        {
            using (var db = new ReqAppContext())
            {
                var result = from roleReq in db.RoleRequests
                                join req in db.Requests on roleReq.RequestID equals req.ID
                                where roleReq.RoleID == role.ID
                                select req;
                return result.ToList();
            }            
        }

        public void LoadRoles(Request request)
        {
            using (var db = new ReqAppContext())
            {
                request.Roles = from roleReq in db.RoleRequests
                                    join role in db.Roles on roleReq.RoleID equals role.ID
                                    where roleReq.RequestID == request.ID
                                    select role;
            }                        
        }


        public IEnumerable<Request> FindAllOrderResolved(bool desc) 
        {
            using (var db = new ReqAppContext())
            {
                return desc ?
                    db.Requests.OrderByDescending(r => r.Resolved).ToList() :
                    db.Requests.OrderBy(r => r.Resolved).ToList();
            }                        
        }




        public IEnumerable<Request> FindByRoleOrderResolved(Role role, bool desc) 
        { 
            using (var db = new ReqAppContext())
            {
                var sql = new StringBuilder(
@"SELECT *
FROM dbo.Requests AS req
    INNER JOIN dbo.RoleRequests AS roleReq ON roleReq.RequestID = req.ID AND roleReq.RoleID = @roleID    
ORDER BY req.Resolved");

                if (desc) sql.Append(" DESC");

                return db.Requests.SqlQuery(sql.ToString(), new SqlParameter("@roleID", role.ID)).ToList();
            }
        }


        public IEnumerable<Request> FindByRoleOrderResolvedBy(Role role, bool desc)
        {
            using (var db = new ReqAppContext())
            {
                var sql = new StringBuilder(
@"SELECT 
    req.*, 
    (SELECT COUNT(*) 
        FROM dbo.Resolutions AS res 
        WHERE res.RequestID = req.ID AND res.RoleID = @roleID) AS order_column
FROM dbo.Requests AS req
    INNER JOIN dbo.RoleRequests AS roleReq ON roleReq.RequestID = req.ID AND roleReq.RoleID = @roleID    
ORDER BY order_column");

                if (desc) sql.Append(" DESC");
                
                return db.Requests.SqlQuery(sql.ToString(), new SqlParameter("@roleID", role.ID)).ToList();
            }
        }
    }
}