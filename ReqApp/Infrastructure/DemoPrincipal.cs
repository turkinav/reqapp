﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Web.SessionState;

namespace ReqApp.Infrastructure
{
    public class DemoPrincipal : IPrincipal
    {
        public IIdentity Identity { get; private set; }

        public bool IsInRole(string roleName)
        {
            return roleName == Identity.Name;
        }

        public DemoPrincipal(string name)
        {
            Identity = new GenericIdentity(name);
        }
    }
}