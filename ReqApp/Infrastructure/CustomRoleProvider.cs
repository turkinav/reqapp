﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using ReqApp.Repositories;

namespace ReqApp.Infrastructure
{
    public class CustomRoleProvider : RoleProvider
    {
        //TODO: использовать встраивание зависимостей
        private IRoleRepository roleRepository = new ReqApp.Repositories.EF.RoleRepository();

        public override bool IsUserInRole(string username, string roleName)
        {
            return username == roleName;
        }

        public override string[] GetRolesForUser(string username)
        {

            var role = roleRepository.FindByName(username);
            return role != null ? new string[] { role.Name } : new string[] { };            
        }
        
        public override string[] GetAllRoles()
        {                        
            return roleRepository.All.Select(r => r.Name).ToArray();            
        }

        public override string ApplicationName { get; set; }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames) 
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName) 
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole) 
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch) 
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName) 
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames) 
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName) 
        {
            throw new NotImplementedException();
        }        
    }
}