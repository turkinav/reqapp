﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReqApp.BusinessLogic.Models;

namespace ReqApp.Infrastructure.Persistence.EF
{
    public class ReqAppInitializer: System.Data.Entity.DropCreateDatabaseIfModelChanges<ReqAppContext>
    {
        protected override void Seed(ReqAppContext context)
        {
            var roles = new List<Role>
            {
            new Role{Name="Секретарь"},
            new Role{Name="Бухгалтер"},
            new Role{Name="Менеджер закупок"},
            new Role{Name="Аналитик"},
            new Role{Name="Руководитель"}
            };
            roles.ForEach(role => context.Roles.Add(role));
            context.SaveChanges();
        }
    }
}