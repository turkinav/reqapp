﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ReqApp.BusinessLogic.Models;

namespace ReqApp.Infrastructure.Persistence.EF
{
    public class ReqAppContext: DbContext
    {

        public ReqAppContext() : base("ReqAppContext")
        {}

        public DbSet<Role> Roles { get; set; }        
        public DbSet<Request> Requests { get; set; }
        public DbSet<RoleRequest> RoleRequests { get; set; }
        public DbSet<Resolution> Resolutions { get; set; }
    }
}