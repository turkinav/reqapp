﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReqApp.BusinessLogic.Models
{
    public class EntityBase 
    {
        public int ID { get; set; }

        public override bool Equals(Object other)
        {
            return other != null && (Object.ReferenceEquals(this, other) ||
                (other.GetType() == this.GetType()) && ((other as EntityBase).ID == ID));
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ ID;
        }

        public static bool operator ==(EntityBase a, EntityBase b)
        {          
            // If both null, return true
            if ((Object)a == null && (Object)b == null)
            {
                return true;
            }
            
            return ((Object)a != null) ? a.Equals(b) : b.Equals(a);
        }

        public static bool operator !=(EntityBase a, EntityBase b)
        {
            return !(a == b);
        }
    }
}