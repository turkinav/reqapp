﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReqApp.BusinessLogic.Models
{
    public class RoleRequest
    {
        public int ID { get; set; }

        [Required]
        [Index("IX_RoleIDRequestID", 1, IsUnique = true)]
        public int RoleID { get; set; }

        [Required]
        [Index("IX_RoleIDRequestID", 2, IsUnique = true)]
        public int RequestID { get; set; }
    }
}