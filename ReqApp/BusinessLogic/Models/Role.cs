﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReqApp.BusinessLogic.Models
{    
    public class Role : EntityBase
    {
        [Required]
        [StringLength(60, MinimumLength = 3)]
        [Index("NameIndex", IsUnique = true)]
        public string Name { get; set; }                
    }
}