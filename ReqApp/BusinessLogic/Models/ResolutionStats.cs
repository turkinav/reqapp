﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ReqApp.BusinessLogic.Models
{
    public class ResolutionStats
    {
        [Display(Name = "Роль")]
        public string RoleName { get; set; }

        [Display(Name="Среднее время рассмотрения заявки в часах")]
        [DisplayFormat(DataFormatString = "{0:0.###}")]
        public double? AvgLatencyHours { get; set; } 
    }
}