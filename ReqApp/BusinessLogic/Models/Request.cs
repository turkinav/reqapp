﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReqApp.BusinessLogic.Models
{
    public enum RequestPriority
    {
        [Display(Name = "Низкий")]
        Low = 0,

        [Display(Name = "Средний")]
        Medium = 1,

        [Display(Name = "Высокий")]
        High = 2
    };

    public class Request : EntityBase
    {        
        private HashSet<Role> roles;
        private HashSet<Resolution> resolutions;

        public Request() 
        {
            RegistrationTime = DateTime.Now;            
        }

        [Required]
        [Index("TitleIndex", IsUnique = true)]        
        [StringLength(60)]
        public string Title { get; set; }
                
        [StringLength(255)]
        public string Desc { get; set; }
        
        public RequestPriority Priority { get; set; }
        
        [Required]
        [Column(TypeName = "DateTime2")]
        public DateTime RegistrationTime { get; set; }

        [Required]                       
        [Column(TypeName = "DateTime2")]
        public DateTime? DueDate { get; set; }        
        
        public bool Resolved { get; set; }

        public IEnumerable<Role> Roles 
        {
            get { return roles ?? (roles = new HashSet<Role>()); }
            set { roles = new HashSet<Role>(value); }
        }

        public virtual IEnumerable<Resolution> Resolutions 
        {
            get { return resolutions ?? (Resolutions = new HashSet<Resolution>()); }
            set { resolutions = new HashSet<Resolution>(value); }        
        }
                
        public bool IsResolvedBy(Role role) 
        {
            return Resolutions.Any(r => r.RoleID == role.ID);
        }

        public bool CanBeResolvedBy(Role role) 
        {
            var result = Roles.Any(r => r == role) && !IsResolvedBy(role);

            if (result && (role.Name == "Руководитель"))
            {
                // Руководитель решает последним
                result = LastResolutionLeft;
            }

            return result;
        }

        public bool LastResolutionLeft 
        {
            get { return Roles.Count() == Resolutions.Count() + 1; }
        }
    }
}