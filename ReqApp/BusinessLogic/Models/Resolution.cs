﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReqApp.BusinessLogic.Models
{
    public enum ResolutionResult 
    {
        [Display(Name="Не согласовано")]
        NotReconciled = 0,

        [Display(Name = "Согласовано")]
        Reconciled = 1,

        [Display(Name = "Не требует согласования")]
        ReconcilationNotRequired = 2
    }

    public class Resolution : EntityBase
    {
        public ResolutionResult? Result { get; set; }
        
        [StringLength(255)]
        public string Desc { get; set; }

        [Required]        
        [ForeignKey("Request")]
        [Index("IX_RoleIDRequestID", 1, IsUnique = true)]        
        public int? RequestID { get; set; }

        [Required]
        [ForeignKey("Role")]
        [Index("IX_RoleIDRequestID", 2, IsUnique = true)]
        public virtual int? RoleID { get; set; }

        [Required]                
        [Column(TypeName = "DateTime2")]
        public DateTime RegistrationTime { get; set; }

        public double LatencyHours{ get; set; }
        
        public Request Request { get; set; }

        public Role Role { get; set; }
    }
}