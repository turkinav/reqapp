﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReqApp.BusinessLogic.Models;
using ReqApp.Repositories;

namespace ReqApp.BusinessLogic.Services
{
    public class RequestDispatcher
    {
        //TODO: использовать встраивание зависимостей
        private IRoleRepository roleRepository = new ReqApp.Repositories.EF.RoleRepository();
        private IRequestRepository requestRepository = new ReqApp.Repositories.EF.RequestRepository();
        private IResolutionRepository resolutionRepository = new ReqApp.Repositories.EF.ResolutionRepository();


        public Request RegisterRequest(string title, string desc, RequestPriority priority, DateTime dueDate, int[] roleIDs) 
        {
            if (String.IsNullOrEmpty(title))
            { 
                throw new ArgumentException("title");
            }

            roleIDs = roleIDs ?? new int[] { };

            var request = new Request 
            { 
                Title = title,
                Desc = desc,
                Priority = priority,
                RegistrationTime = DateTime.Now,
                DueDate = dueDate                
            };
            var roles = roleRepository.FindByIDs(roleIDs).ToList();
            // Руководитель должен быть всегда
            roles.Add(roleRepository.FindByName("Руководитель"));
            request.Roles = roles;
            
            requestRepository.Add(request);

            return request;
        }
        

        public Resolution RegisterResolution(int requestID, int roleID, ResolutionResult result, string description) 
        {
            // TODO: Использовать фабрику
            var request = requestRepository.GetByID(requestID);
            if (request == null)
            {
                // Заявка не найдена
                throw new InvalidOperationException();
            }
            requestRepository.LoadRoles(request);
            request.Resolutions = resolutionRepository.FindByRequest(request.ID);            

            var role = roleRepository.GetByID(roleID);
            if (!request.CanBeResolvedBy(role))
            {                
                throw new InvalidOperationException();
            }

            request.Resolved = request.LastResolutionLeft;

            var resolution = new Resolution 
            { 
                RequestID = request.ID, 
                RoleID = role.ID,
                Result = result,
                Desc = description,
                RegistrationTime = DateTime.Now
            };
            resolution.LatencyHours = (resolution.RegistrationTime - request.RegistrationTime).TotalHours;
            
            // TODO: использовать транзакцию
            resolutionRepository.Add(resolution);            
            if (request.Resolved)
            {
                requestRepository.Update(request);
            }

            return resolution;
        }
    }
}