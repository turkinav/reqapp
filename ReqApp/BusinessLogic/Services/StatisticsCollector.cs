﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data.Entity.Core.EntityClient;
using ReqApp.BusinessLogic.Models;
using ReqApp.Infrastructure.Persistence.EF;

namespace ReqApp.BusinessLogic.Services
{
    public class StatisticsCollector
    {
        public readonly static DateTime MaxDate = new DateTime(DateTime.Today.Year + 1, 1, 1);
        public readonly static DateTime MinDate = new DateTime(MaxDate.Year - 5, 1, 1);
        
        public  IEnumerable<ResolutionStats> GetStatistics(DateTime dateFrom, DateTime dateTo)
        {
            if ((dateFrom < MinDate) || (MaxDate < dateTo))
            {
                throw new ArgumentException("date range");
            }

            //using (var connection = new EntityConnection("name=ReqAppContext"))
            using (var connection = new ReqAppContext().Database.Connection)
            {
                var cmd = connection.CreateCommand();

                cmd.CommandText = @"SELECT role.Name, AVG(res.LatencyHours) 
    FROM Roles as role 
        LEFT JOIN Resolutions AS res ON res.RoleID = role.ID 
	    AND CAST(res.RegistrationTime AS DATE) BETWEEN @dateFrom AND @dateTo
    GROUP BY role.Name";

                AddCmdParameter<DateTime>(cmd, "@dateFrom", dateFrom.Date);
                AddCmdParameter<DateTime>(cmd, "@dateTo", dateTo.Date);

                var result = new List<ResolutionStats>();

                connection.Open();

                using (DbDataReader reader = cmd.ExecuteReader(
                    CommandBehavior.SequentialAccess | CommandBehavior.CloseConnection))
                {                    
                    while (reader.Read())
                    {                        
                        var stats = new ResolutionStats 
                        { 
                            RoleName = reader.GetString(0), 
                            AvgLatencyHours =  reader.IsDBNull(1) ? null : (double?)reader.GetDouble(1)
                        };
                        //System.Diagnostics.Debug.WriteLine(reader.GetValue(1).GetType().Name);
                        result.Add(stats);
                    }
                }

                return result;
            }            
        }

        private void AddCmdParameter<T>(DbCommand cmd, string name, T value)
        {
            var p = cmd.CreateParameter();
            p.ParameterName = name;
            p.Value = value;

            cmd.Parameters.Add(p);
        }
    }
}